# LibreWolf - Browser

1. Install LibreWolf (best option) or FireFox/other forks.
2. Copy [user.js](user.js) in root of profile in Librewolf.
3. Launch LibreWolf with this new profile and close LibreWolf.
4. Delete `customizableui-special-spring` in `prefs.js` in root of profile.
5. Launch LibreWolf and install Sidebery and import [config](sidebery-data.json)
6. Copy [chrome](chrome) in root of profile in LibreWolf. Restart LibreWolf

## Demo

![demo](demo.gif)